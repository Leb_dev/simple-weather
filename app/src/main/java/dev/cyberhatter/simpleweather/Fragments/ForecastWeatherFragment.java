package dev.cyberhatter.simpleweather.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dev.cyberhatter.simpleweather.Global.AccuWeather;
import dev.cyberhatter.simpleweather.Global.RetrofitClient;
import dev.cyberhatter.simpleweather.Global.Vars;
import dev.cyberhatter.simpleweather.Model.FiveDaysForecastInfo;
import dev.cyberhatter.simpleweather.Model.HalfDayForecastInfo;
import dev.cyberhatter.simpleweather.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class ForecastWeatherFragment extends Fragment {

    @BindViews({R.id.forecast_halfDay_image1, R.id.forecast_halfDay_image2,
            R.id.forecast_halfDay_image3, R.id.forecast_halfDay_image4,
            R.id.forecast_halfDay_image5, R.id.forecast_halfDay_image6,
            R.id.forecast_halfDay_image7, R.id.forecast_halfDay_image8,
            R.id.forecast_halfDay_image9, R.id.forecast_halfDay_image10,
            R.id.forecast_halfDay_image11, R.id.forecast_halfDay_image12
    })
    List<ImageView> halfDayForecastImages;
    @BindViews({R.id.forecast_halfDay_temperatureText1, R.id.forecast_halfDay_temperatureText2,
            R.id.forecast_halfDay_temperatureText3, R.id.forecast_halfDay_temperatureText4,
            R.id.forecast_halfDay_temperatureText5, R.id.forecast_halfDay_temperatureText6,
            R.id.forecast_halfDay_temperatureText7, R.id.forecast_halfDay_temperatureText8,
            R.id.forecast_halfDay_temperatureText9, R.id.forecast_halfDay_temperatureText10,
            R.id.forecast_halfDay_temperatureText11, R.id.forecast_halfDay_temperatureText12})
    List<TextView> halfDayForecastTemperatures;
    @BindViews({R.id.forecast_halfDay_timeText1, R.id.forecast_halfDay_timeText2,
            R.id.forecast_halfDay_timeText3, R.id.forecast_halfDay_timeText4,
            R.id.forecast_halfDay_timeText5, R.id.forecast_halfDay_timeText6,
            R.id.forecast_halfDay_timeText7, R.id.forecast_halfDay_timeText8,
            R.id.forecast_halfDay_timeText9, R.id.forecast_halfDay_timeText10,
            R.id.forecast_halfDay_timeText11, R.id.forecast_halfDay_timeText12})
    List<TextView> halfDayForecastTimes;
    @BindViews({R.id.forecast_fiveDays_day1, R.id.forecast_fiveDays_day2,
            R.id.forecast_fiveDays_day3, R.id.forecast_fiveDays_day4, R.id.forecast_fiveDays_day5})
    List<ImageView> fiveDaysDays;
    @BindViews({R.id.forecast_fiveDays_night1, R.id.forecast_fiveDays_night2,
            R.id.forecast_fiveDays_night3, R.id.forecast_fiveDays_night4, R.id.forecast_fiveDays_night5})
    List<ImageView> fiveDaysNights;
    @BindViews({R.id.forecast_fiveDays_max1, R.id.forecast_fiveDays_max2,
            R.id.forecast_fiveDays_max3, R.id.forecast_fiveDays_max4, R.id.forecast_fiveDays_max5})
    List<TextView> fiveDaysMaxs;
    @BindViews({R.id.forecast_fiveDays_min1, R.id.forecast_fiveDays_min2,
            R.id.forecast_fiveDays_min3, R.id.forecast_fiveDays_min4, R.id.forecast_fiveDays_min5})
    List<TextView> fiveDaysMins;
    @BindViews({R.id.forecast_fiveDays_time1, R.id.forecast_fiveDays_time2,
            R.id.forecast_fiveDays_time3, R.id.forecast_fiveDays_time4, R.id.forecast_fiveDays_time5})
    List<TextView> fiveDaysTimes;
    @BindView(R.id.forecast_halfDayCard)
    View halfDayCard;
    @BindView(R.id.forecast_fiveDaysCard)
    View fiveDaysCard;
    @BindView(R.id.forecast_progressBar)
    ProgressBar progressBar;

    private CompositeDisposable compositeDisposable;
    private AccuWeather accuWeather;
    private Unbinder unbinder;

    public ForecastWeatherFragment() {
        compositeDisposable = new CompositeDisposable();
        Retrofit retrofit = RetrofitClient.getClient();
        accuWeather = retrofit.create(AccuWeather.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forecast, container, false);
        unbinder = ButterKnife.bind(this, view);

        halfDayCard.setVisibility(View.GONE);
        fiveDaysCard.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        compositeDisposable.add(
                accuWeather.get12HourForecast(Vars.locationInfo.getKey(), Vars.API_KEY, true)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                halfDayForecastInfos -> {
                                    halfDayCard.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                    setHalfDayForecast(halfDayForecastInfos);
                                },
                                throwable -> {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                        )
        );
        compositeDisposable.add(
                accuWeather.get5DaysForecast(Vars.locationInfo.getKey(), Vars.API_KEY, true)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                fiveDaysForecastInfo -> {
                                    fiveDaysCard.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                    setFiveDaysForecast(fiveDaysForecastInfo);
                                },
                                throwable -> {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                        )
        );
        return view;
    }

    private void setFiveDaysForecast(FiveDaysForecastInfo fiveDaysForecastInfo) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd", Locale.getDefault());
        for (int i = 0; i < 5; i++) {
            String max = fiveDaysForecastInfo.getDailyForecasts()[i].getTemperature().getMaximum().getValue().toString();
            String min = fiveDaysForecastInfo.getDailyForecasts()[i].getTemperature().getMinimum().getValue().toString();
            Integer day = fiveDaysForecastInfo.getDailyForecasts()[i].getDay().getIcon();
            Integer night = fiveDaysForecastInfo.getDailyForecasts()[i].getNight().getIcon();
            Date date = new Date(fiveDaysForecastInfo.getDailyForecasts()[i].getEpochDate() * 1000);
            Picasso.get()
                    .load(
                            "https://developer.accuweather.com/sites/default/files/" +
                                    (day >= 10 ? day : "0" + day)
                                    + "-s.png"
                    )
                    .into(fiveDaysDays.get(i));
            Picasso.get()
                    .load(
                            "https://developer.accuweather.com/sites/default/files/" +
                                    (night >= 10 ? night : "0" + night)
                                    + "-s.png"
                    )
                    .into(fiveDaysNights.get(i));
            fiveDaysMaxs.get(i).setText(max);
            fiveDaysMins.get(i).setText(min);
            fiveDaysTimes.get(i).setText(simpleDateFormat.format(date));
        }
    }

    private void setHalfDayForecast(HalfDayForecastInfo[] halfDayForecastInfos) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        for (int i = 0; i < 12; i++) {
            Picasso.get()
                    .load(
                            "https://developer.accuweather.com/sites/default/files/" +
                                    (halfDayForecastInfos[i].getWeatherIcon() >= 10 ? halfDayForecastInfos[i].getWeatherIcon() : "0" + halfDayForecastInfos[i].getWeatherIcon())
                                    + "-s.png"
                    )
                    .into(halfDayForecastImages.get(i));
            halfDayForecastTemperatures.get(i).setText(halfDayForecastInfos[i].getTemperature().getValue().toString());
            halfDayForecastTimes.get(i).setText(simpleDateFormat.format(new Date(halfDayForecastInfos[i].getEpochDateTime() * 1000)));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        compositeDisposable.clear();
    }
}