package dev.cyberhatter.simpleweather.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dev.cyberhatter.simpleweather.Global.AccuWeather;
import dev.cyberhatter.simpleweather.Global.RetrofitClient;
import dev.cyberhatter.simpleweather.Global.Vars;
import dev.cyberhatter.simpleweather.Model.CurrentWeatherInfo;
import dev.cyberhatter.simpleweather.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class CurrentWeatherFragment extends Fragment {

    @BindView(R.id.currentConditions_conditionsCard)
    View conditionsCard;
    @BindView(R.id.currentConditions_progressBar)
    ProgressBar progressBar;
    @BindView(R.id.currentConditions_weatherIcon)
    ImageView weatherIcon;
    @BindView(R.id.currentConditions_locationText)
    TextView locationText;
    @BindView(R.id.currentConditions_timeText)
    TextView timeText;
    @BindView(R.id.currentConditions_weatherText)
    TextView weatherText;
    @BindView(R.id.currentConditions_temperatureCelsius)
    TextView celsiusText;
    @BindView(R.id.currentConditions_temperatureFahrenheit)
    TextView fahrenheitText;

    private CompositeDisposable compositeDisposable;
    private AccuWeather accuWeather;
    private Unbinder unbinder;

    public CurrentWeatherFragment() {
        compositeDisposable = new CompositeDisposable();
        Retrofit retrofit = RetrofitClient.getClient();
        accuWeather = retrofit.create(AccuWeather.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_current_conditions, container, false);
        unbinder = ButterKnife.bind(this, view);

        conditionsCard.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        compositeDisposable.add(
                accuWeather.getWeatherInfo(Vars.locationInfo.getKey(), Vars.API_KEY)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                currentWeatherInfos -> {
                                    progressBar.setVisibility(View.GONE);
                                    setWeather(currentWeatherInfos[0]);
                                },
                                throwable -> {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                        )
        );
        return view;
    }

    private void setWeather(CurrentWeatherInfo currentWeatherInfo) {
        Picasso.get()
                .load(
                        "https://developer.accuweather.com/sites/default/files/" +
                                (currentWeatherInfo.getWeatherIcon() >= 10 ? currentWeatherInfo.getWeatherIcon() : "0" + currentWeatherInfo.getWeatherIcon())
                                + "-s.png"
                )
                .into(weatherIcon, new Callback() {
                    @Override
                    public void onSuccess() {
                        weatherIcon.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError(Exception e) {
                        weatherIcon.setVisibility(View.GONE);
                    }
                });
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm, dd/MMMM/YYYY", Locale.getDefault());
        locationText.setText(Vars.locationInfo.getLocalizedName());
        timeText.setText(simpleDateFormat.format(new Date(currentWeatherInfo.getEpochTime() * 1000)));
        weatherText.setText(currentWeatherInfo.getWeatherText());
        celsiusText.setText(currentWeatherInfo.getTemperature().getMetric().getValue().toString());
        fahrenheitText.setText(currentWeatherInfo.getTemperature().getImperial().getValue().toString());
        conditionsCard.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        compositeDisposable.clear();
    }
}
