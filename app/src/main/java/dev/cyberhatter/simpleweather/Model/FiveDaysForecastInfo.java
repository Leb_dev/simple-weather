package dev.cyberhatter.simpleweather.Model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class FiveDaysForecastInfo {

    DailyForecast[] DailyForecasts;

    @NoArgsConstructor
    @Getter
    @Setter
    public class DailyForecast {

        Long EpochDate;
        Temperature Temperature;
        TimeOfDayIcon Day;
        TimeOfDayIcon Night;

        @NoArgsConstructor
        @Getter
        @Setter
        public class Temperature {

            TemperatureValue Minimum;
            TemperatureValue Maximum;

            @NoArgsConstructor
            @Setter
            @Getter
            public class TemperatureValue {

                Double Value;
            }
        }

        @NoArgsConstructor
        @Getter
        @Setter
        public class TimeOfDayIcon {

            Integer Icon;
        }
    }
}
/* Response
{
  "Headline": {
    "EffectiveDate": "2019-01-26T19:00:00+01:00",
    "EffectiveEpochDate": 1548525600,
    "Severity": 3,
    "Text": "Expect showery weather Saturday evening through Sunday afternoon",
    "Category": "rain",
    "EndDate": "2019-01-27T19:00:00+01:00",
    "EndEpochDate": 1548612000,
    "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/extended-weather-forecast/175413?unit=c&lang=en-us",
    "Link": "http://www.accuweather.com/en/de/nauroth/57583/daily-weather-forecast/175413?unit=c&lang=en-us"
  },
  "DailyForecasts": [
    {
      "Date": "2019-01-26T07:00:00+01:00",
      "EpochDate": 1548482400,
      "Temperature": {
        "Minimum": {
          "Value": 1.6,
          "Unit": "C",
          "UnitType": 17
        },
        "Maximum": {
          "Value": 6.1,
          "Unit": "C",
          "UnitType": 17
        }
      },
      "Day": {
        "Icon": 12,
        "IconPhrase": "Showers"
      },
      "Night": {
        "Icon": 12,
        "IconPhrase": "Showers"
      },
      "Sources": [
        "AccuWeather"
      ],
      "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/daily-weather-forecast/175413?day=1&unit=c&lang=en-us",
      "Link": "http://www.accuweather.com/en/de/nauroth/57583/daily-weather-forecast/175413?day=1&unit=c&lang=en-us"
    },
    {
      "Date": "2019-01-27T07:00:00+01:00",
      "EpochDate": 1548568800,
      "Temperature": {
        "Minimum": {
          "Value": -0.1,
          "Unit": "C",
          "UnitType": 17
        },
        "Maximum": {
          "Value": 3.3,
          "Unit": "C",
          "UnitType": 17
        }
      },
      "Day": {
        "Icon": 18,
        "IconPhrase": "Rain"
      },
      "Night": {
        "Icon": 29,
        "IconPhrase": "Rain and snow"
      },
      "Sources": [
        "AccuWeather"
      ],
      "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/daily-weather-forecast/175413?day=2&unit=c&lang=en-us",
      "Link": "http://www.accuweather.com/en/de/nauroth/57583/daily-weather-forecast/175413?day=2&unit=c&lang=en-us"
    },
    {
      "Date": "2019-01-28T07:00:00+01:00",
      "EpochDate": 1548655200,
      "Temperature": {
        "Minimum": {
          "Value": -4.3,
          "Unit": "C",
          "UnitType": 17
        },
        "Maximum": {
          "Value": 0.6,
          "Unit": "C",
          "UnitType": 17
        }
      },
      "Day": {
        "Icon": 29,
        "IconPhrase": "Rain and snow"
      },
      "Night": {
        "Icon": 19,
        "IconPhrase": "Flurries"
      },
      "Sources": [
        "AccuWeather"
      ],
      "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/daily-weather-forecast/175413?day=3&unit=c&lang=en-us",
      "Link": "http://www.accuweather.com/en/de/nauroth/57583/daily-weather-forecast/175413?day=3&unit=c&lang=en-us"
    },
    {
      "Date": "2019-01-29T07:00:00+01:00",
      "EpochDate": 1548741600,
      "Temperature": {
        "Minimum": {
          "Value": -4.4,
          "Unit": "C",
          "UnitType": 17
        },
        "Maximum": {
          "Value": -0.2,
          "Unit": "C",
          "UnitType": 17
        }
      },
      "Day": {
        "Icon": 19,
        "IconPhrase": "Flurries"
      },
      "Night": {
        "Icon": 7,
        "IconPhrase": "Cloudy"
      },
      "Sources": [
        "AccuWeather"
      ],
      "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/daily-weather-forecast/175413?day=4&unit=c&lang=en-us",
      "Link": "http://www.accuweather.com/en/de/nauroth/57583/daily-weather-forecast/175413?day=4&unit=c&lang=en-us"
    },
    {
      "Date": "2019-01-30T07:00:00+01:00",
      "EpochDate": 1548828000,
      "Temperature": {
        "Minimum": {
          "Value": -5.2,
          "Unit": "C",
          "UnitType": 17
        },
        "Maximum": {
          "Value": -0.2,
          "Unit": "C",
          "UnitType": 17
        }
      },
      "Day": {
        "Icon": 20,
        "IconPhrase": "Mostly cloudy w/ flurries"
      },
      "Night": {
        "Icon": 38,
        "IconPhrase": "Mostly cloudy"
      },
      "Sources": [
        "AccuWeather"
      ],
      "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/daily-weather-forecast/175413?day=5&unit=c&lang=en-us",
      "Link": "http://www.accuweather.com/en/de/nauroth/57583/daily-weather-forecast/175413?day=5&unit=c&lang=en-us"
    }
  ]
}
*/