package dev.cyberhatter.simpleweather.Model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class HalfDayForecastInfo {

    Long EpochDateTime;
    Integer WeatherIcon;
    String IconPhrase;
    Temperature Temperature;

    @NoArgsConstructor
    @Getter
    @Setter
    public class Temperature {

        Double Value;
        String Unit;
        Integer UnitType;
    }
}
/* Response
[
  {
    "DateTime": "2019-01-26T16:00:00+01:00",
    "EpochDateTime": 1548514800,
    "WeatherIcon": 7,
    "IconPhrase": "Cloudy",
    "IsDaylight": true,
    "Temperature": {
      "Value": 2.2,
      "Unit": "C",
      "UnitType": 17
    },
    "PrecipitationProbability": 32,
    "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&unit=c&lang=en-us",
    "Link": "http://www.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&hbhhour=16&unit=c&lang=en-us"
  },
  {
    "DateTime": "2019-01-26T17:00:00+01:00",
    "EpochDateTime": 1548518400,
    "WeatherIcon": 7,
    "IconPhrase": "Cloudy",
    "IsDaylight": true,
    "Temperature": {
      "Value": 2.1,
      "Unit": "C",
      "UnitType": 17
    },
    "PrecipitationProbability": 32,
    "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&unit=c&lang=en-us",
    "Link": "http://www.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&hbhhour=17&unit=c&lang=en-us"
  },
  {
    "DateTime": "2019-01-26T18:00:00+01:00",
    "EpochDateTime": 1548522000,
    "WeatherIcon": 7,
    "IconPhrase": "Cloudy",
    "IsDaylight": false,
    "Temperature": {
      "Value": 1.8,
      "Unit": "C",
      "UnitType": 17
    },
    "PrecipitationProbability": 36,
    "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&unit=c&lang=en-us",
    "Link": "http://www.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&hbhhour=18&unit=c&lang=en-us"
  },
  {
    "DateTime": "2019-01-26T19:00:00+01:00",
    "EpochDateTime": 1548525600,
    "WeatherIcon": 7,
    "IconPhrase": "Cloudy",
    "IsDaylight": false,
    "Temperature": {
      "Value": 1.6,
      "Unit": "C",
      "UnitType": 17
    },
    "PrecipitationProbability": 43,
    "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&unit=c&lang=en-us",
    "Link": "http://www.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&hbhhour=19&unit=c&lang=en-us"
  },
  {
    "DateTime": "2019-01-26T20:00:00+01:00",
    "EpochDateTime": 1548529200,
    "WeatherIcon": 7,
    "IconPhrase": "Cloudy",
    "IsDaylight": false,
    "Temperature": {
      "Value": 1.9,
      "Unit": "C",
      "UnitType": 17
    },
    "PrecipitationProbability": 47,
    "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&unit=c&lang=en-us",
    "Link": "http://www.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&hbhhour=20&unit=c&lang=en-us"
  },
  {
    "DateTime": "2019-01-26T21:00:00+01:00",
    "EpochDateTime": 1548532800,
    "WeatherIcon": 12,
    "IconPhrase": "Showers",
    "IsDaylight": false,
    "Temperature": {
      "Value": 2,
      "Unit": "C",
      "UnitType": 17
    },
    "PrecipitationProbability": 51,
    "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&unit=c&lang=en-us",
    "Link": "http://www.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&hbhhour=21&unit=c&lang=en-us"
  },
  {
    "DateTime": "2019-01-26T22:00:00+01:00",
    "EpochDateTime": 1548536400,
    "WeatherIcon": 7,
    "IconPhrase": "Cloudy",
    "IsDaylight": false,
    "Temperature": {
      "Value": 2.1,
      "Unit": "C",
      "UnitType": 17
    },
    "PrecipitationProbability": 40,
    "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&unit=c&lang=en-us",
    "Link": "http://www.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&hbhhour=22&unit=c&lang=en-us"
  },
  {
    "DateTime": "2019-01-26T23:00:00+01:00",
    "EpochDateTime": 1548540000,
    "WeatherIcon": 7,
    "IconPhrase": "Cloudy",
    "IsDaylight": false,
    "Temperature": {
      "Value": 2,
      "Unit": "C",
      "UnitType": 17
    },
    "PrecipitationProbability": 34,
    "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&unit=c&lang=en-us",
    "Link": "http://www.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=1&hbhhour=23&unit=c&lang=en-us"
  },
  {
    "DateTime": "2019-01-27T00:00:00+01:00",
    "EpochDateTime": 1548543600,
    "WeatherIcon": 7,
    "IconPhrase": "Cloudy",
    "IsDaylight": false,
    "Temperature": {
      "Value": 2,
      "Unit": "C",
      "UnitType": 17
    },
    "PrecipitationProbability": 34,
    "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=2&unit=c&lang=en-us",
    "Link": "http://www.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=2&hbhhour=0&unit=c&lang=en-us"
  },
  {
    "DateTime": "2019-01-27T01:00:00+01:00",
    "EpochDateTime": 1548547200,
    "WeatherIcon": 7,
    "IconPhrase": "Cloudy",
    "IsDaylight": false,
    "Temperature": {
      "Value": 2,
      "Unit": "C",
      "UnitType": 17
    },
    "PrecipitationProbability": 34,
    "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=2&unit=c&lang=en-us",
    "Link": "http://www.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=2&hbhhour=1&unit=c&lang=en-us"
  },
  {
    "DateTime": "2019-01-27T02:00:00+01:00",
    "EpochDateTime": 1548550800,
    "WeatherIcon": 7,
    "IconPhrase": "Cloudy",
    "IsDaylight": false,
    "Temperature": {
      "Value": 2.2,
      "Unit": "C",
      "UnitType": 17
    },
    "PrecipitationProbability": 34,
    "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=2&unit=c&lang=en-us",
    "Link": "http://www.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=2&hbhhour=2&unit=c&lang=en-us"
  },
  {
    "DateTime": "2019-01-27T03:00:00+01:00",
    "EpochDateTime": 1548554400,
    "WeatherIcon": 7,
    "IconPhrase": "Cloudy",
    "IsDaylight": false,
    "Temperature": {
      "Value": 2.3,
      "Unit": "C",
      "UnitType": 17
    },
    "PrecipitationProbability": 40,
    "MobileLink": "http://m.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=2&unit=c&lang=en-us",
    "Link": "http://www.accuweather.com/en/de/nauroth/57583/hourly-weather-forecast/175413?day=2&hbhhour=3&unit=c&lang=en-us"
  }
]
*/