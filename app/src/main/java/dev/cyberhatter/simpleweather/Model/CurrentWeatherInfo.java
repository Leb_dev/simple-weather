package dev.cyberhatter.simpleweather.Model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CurrentWeatherInfo {

    Long EpochTime;
    String WeatherText;
    Integer WeatherIcon;
    Temperature Temperature;

    @NoArgsConstructor
    @Getter
    @Setter
    public class Temperature {

        Metric Metric;
        Imperial Imperial;

        @NoArgsConstructor
        @Getter
        @Setter
        public class Metric {

            Double Value;
            String Unit;
            Integer UnitType;
        }

        @NoArgsConstructor
        @Setter
        @Getter
        public class Imperial {

            Double Value;
            String Unit;
            Integer UnitType;
        }
    }
}
/* Response
[
  {
    "LocalObservationDateTime": "2019-01-22T19:40:00+04:30",
    "EpochTime": 1548169800,
    "WeatherText": "Clear",
    "WeatherIcon": 33,
    "HasPrecipitation": false,
    "PrecipitationType": null,
    "IsDayTime": false,
    "Temperature": {
      "Metric": {
        "Value": -4.7,
        "Unit": "C",
        "UnitType": 17
      },
      "Imperial": {
        "Value": 23,
        "Unit": "F",
        "UnitType": 18
      }
    },
    "MobileLink": "http://m.accuweather.com/en/af/tuta/2223/current-weather/2223?lang=en-us",
    "Link": "http://www.accuweather.com/en/af/tuta/2223/current-weather/2223?lang=en-us"
  }
]

detailed:
[
  {
    "LocalObservationDateTime": "2019-01-22T19:50:00+04:30",
    "EpochTime": 1548170400,
    "WeatherText": "Clear",
    "WeatherIcon": 33,
    "HasPrecipitation": false,
    "PrecipitationType": null,
    "IsDayTime": false,
    "Temperature": {
      "Metric": {
        "Value": -4.7,
        "Unit": "C",
        "UnitType": 17
      },
      "Imperial": {
        "Value": 23,
        "Unit": "F",
        "UnitType": 18
      }
    },
    "RealFeelTemperature": {
      "Metric": {
        "Value": -10.1,
        "Unit": "C",
        "UnitType": 17
      },
      "Imperial": {
        "Value": 14,
        "Unit": "F",
        "UnitType": 18
      }
    },
    "RealFeelTemperatureShade": {
      "Metric": {
        "Value": -10.1,
        "Unit": "C",
        "UnitType": 17
      },
      "Imperial": {
        "Value": 14,
        "Unit": "F",
        "UnitType": 18
      }
    },
    "RelativeHumidity": 37,
    "DewPoint": {
      "Metric": {
        "Value": -17.5,
        "Unit": "C",
        "UnitType": 17
      },
      "Imperial": {
        "Value": 0,
        "Unit": "F",
        "UnitType": 18
      }
    },
    "Wind": {
      "Direction": {
        "Degrees": 158,
        "Localized": "SSE",
        "English": "SSE"
      },
      "Speed": {
        "Metric": {
          "Value": 14.7,
          "Unit": "km/h",
          "UnitType": 7
        },
        "Imperial": {
          "Value": 9.2,
          "Unit": "mi/h",
          "UnitType": 9
        }
      }
    },
    "WindGust": {
      "Speed": {
        "Metric": {
          "Value": 21.8,
          "Unit": "km/h",
          "UnitType": 7
        },
        "Imperial": {
          "Value": 13.5,
          "Unit": "mi/h",
          "UnitType": 9
        }
      }
    },
    "UVIndex": 0,
    "UVIndexText": "Low",
    "Visibility": {
      "Metric": {
        "Value": 16.1,
        "Unit": "km",
        "UnitType": 6
      },
      "Imperial": {
        "Value": 10,
        "Unit": "mi",
        "UnitType": 2
      }
    },
    "ObstructionsToVisibility": "",
    "CloudCover": 0,
    "Ceiling": {
      "Metric": {
        "Value": 7376,
        "Unit": "m",
        "UnitType": 5
      },
      "Imperial": {
        "Value": 24200,
        "Unit": "ft",
        "UnitType": 0
      }
    },
    "Pressure": {
      "Metric": {
        "Value": 1019.8,
        "Unit": "mb",
        "UnitType": 14
      },
      "Imperial": {
        "Value": 30.12,
        "Unit": "inHg",
        "UnitType": 12
      }
    },
    "PressureTendency": {
      "LocalizedText": "Rising",
      "Code": "R"
    },
    "Past24HourTemperatureDeparture": {
      "Metric": {
        "Value": 2.1,
        "Unit": "C",
        "UnitType": 17
      },
      "Imperial": {
        "Value": 4,
        "Unit": "F",
        "UnitType": 18
      }
    },
    "ApparentTemperature": {
      "Metric": {
        "Value": -5,
        "Unit": "C",
        "UnitType": 17
      },
      "Imperial": {
        "Value": 23,
        "Unit": "F",
        "UnitType": 18
      }
    },
    "WindChillTemperature": {
      "Metric": {
        "Value": -10.6,
        "Unit": "C",
        "UnitType": 17
      },
      "Imperial": {
        "Value": 13,
        "Unit": "F",
        "UnitType": 18
      }
    },
    "WetBulbTemperature": {
      "Metric": {
        "Value": -7.7,
        "Unit": "C",
        "UnitType": 17
      },
      "Imperial": {
        "Value": 18,
        "Unit": "F",
        "UnitType": 18
      }
    },
    "Precip1hr": {
      "Metric": {
        "Value": 0,
        "Unit": "mm",
        "UnitType": 3
      },
      "Imperial": {
        "Value": 0,
        "Unit": "in",
        "UnitType": 1
      }
    },
    "PrecipitationSummary": {
      "Precipitation": {
        "Metric": {
          "Value": 0,
          "Unit": "mm",
          "UnitType": 3
        },
        "Imperial": {
          "Value": 0,
          "Unit": "in",
          "UnitType": 1
        }
      },
      "PastHour": {
        "Metric": {
          "Value": 0,
          "Unit": "mm",
          "UnitType": 3
        },
        "Imperial": {
          "Value": 0,
          "Unit": "in",
          "UnitType": 1
        }
      },
      "Past3Hours": {
        "Metric": {
          "Value": 0,
          "Unit": "mm",
          "UnitType": 3
        },
        "Imperial": {
          "Value": 0,
          "Unit": "in",
          "UnitType": 1
        }
      },
      "Past6Hours": {
        "Metric": {
          "Value": 0,
          "Unit": "mm",
          "UnitType": 3
        },
        "Imperial": {
          "Value": 0,
          "Unit": "in",
          "UnitType": 1
        }
      },
      "Past9Hours": {
        "Metric": {
          "Value": 0,
          "Unit": "mm",
          "UnitType": 3
        },
        "Imperial": {
          "Value": 0,
          "Unit": "in",
          "UnitType": 1
        }
      },
      "Past12Hours": {
        "Metric": {
          "Value": 0,
          "Unit": "mm",
          "UnitType": 3
        },
        "Imperial": {
          "Value": 0,
          "Unit": "in",
          "UnitType": 1
        }
      },
      "Past18Hours": {
        "Metric": {
          "Value": 0,
          "Unit": "mm",
          "UnitType": 3
        },
        "Imperial": {
          "Value": 0,
          "Unit": "in",
          "UnitType": 1
        }
      },
      "Past24Hours": {
        "Metric": {
          "Value": 0,
          "Unit": "mm",
          "UnitType": 3
        },
        "Imperial": {
          "Value": 0,
          "Unit": "in",
          "UnitType": 1
        }
      }
    },
    "TemperatureSummary": {
      "Past6HourRange": {
        "Minimum": {
          "Metric": {
            "Value": -5.3,
            "Unit": "C",
            "UnitType": 17
          },
          "Imperial": {
            "Value": 22,
            "Unit": "F",
            "UnitType": 18
          }
        },
        "Maximum": {
          "Metric": {
            "Value": -1.6,
            "Unit": "C",
            "UnitType": 17
          },
          "Imperial": {
            "Value": 29,
            "Unit": "F",
            "UnitType": 18
          }
        }
      },
      "Past12HourRange": {
        "Minimum": {
          "Metric": {
            "Value": -10.4,
            "Unit": "C",
            "UnitType": 17
          },
          "Imperial": {
            "Value": 13,
            "Unit": "F",
            "UnitType": 18
          }
        },
        "Maximum": {
          "Metric": {
            "Value": -1.6,
            "Unit": "C",
            "UnitType": 17
          },
          "Imperial": {
            "Value": 29,
            "Unit": "F",
            "UnitType": 18
          }
        }
      },
      "Past24HourRange": {
        "Minimum": {
          "Metric": {
            "Value": -12.4,
            "Unit": "C",
            "UnitType": 17
          },
          "Imperial": {
            "Value": 10,
            "Unit": "F",
            "UnitType": 18
          }
        },
        "Maximum": {
          "Metric": {
            "Value": -1.6,
            "Unit": "C",
            "UnitType": 17
          },
          "Imperial": {
            "Value": 29,
            "Unit": "F",
            "UnitType": 18
          }
        }
      }
    },
    "MobileLink": "http://m.accuweather.com/en/af/tuta/2223/current-weather/2223?lang=en-us",
    "Link": "http://www.accuweather.com/en/af/tuta/2223/current-weather/2223?lang=en-us"
  }
]
*/