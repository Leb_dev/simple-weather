package dev.cyberhatter.simpleweather.Model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @Getter @Setter
public class LocationInfo {

    String Key;
    String LocalizedName;
}
/* Response
{
  "Version": 1,
  "Key": "208091",
  "Type": "City",
  "Rank": 65,
  "LocalizedName": "Hajiabad",
  "EnglishName": "Hajiabad",
  "PrimaryPostalCode": "",
  "Region": {
    "ID": "MEA",
    "LocalizedName": "Middle East",
    "EnglishName": "Middle East"
  },
  "Country": {
    "ID": "IR",
    "LocalizedName": "Iran",
    "EnglishName": "Iran"
  },
  "AdministrativeArea": {
    "ID": "04",
    "LocalizedName": "Esfahan",
    "EnglishName": "Esfahan",
    "Level": 1,
    "LocalizedType": "Province",
    "EnglishType": "Province",
    "CountryID": "IR"
  },
  "TimeZone": {
    "Code": "IRST",
    "Name": "Asia/Tehran",
    "GmtOffset": 3.5,
    "IsDaylightSaving": false,
    "NextOffsetChange": "2019-03-21T20:30:00Z"
  },
  "GeoPosition": {
    "Latitude": 33.082,
    "Longitude": 54.855,
    "Elevation": {
      "Metric": {
        "Value": 972,
        "Unit": "m",
        "UnitType": 5
      },
      "Imperial": {
        "Value": 3188,
        "Unit": "ft",
        "UnitType": 0
      }
    }
  },
  "IsAlias": false,
  "SupplementalAdminAreas": [],
  "DataSets": []
}
 */