package dev.cyberhatter.simpleweather;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Looper;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dev.cyberhatter.simpleweather.Adapters.ViewPagerAdapter;
import dev.cyberhatter.simpleweather.Fragments.CurrentWeatherFragment;
import dev.cyberhatter.simpleweather.Fragments.ForecastWeatherFragment;
import dev.cyberhatter.simpleweather.Global.AccuWeather;
import dev.cyberhatter.simpleweather.Global.RetrofitClient;
import dev.cyberhatter.simpleweather.Global.Vars;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_ACCESS_LOCATION = 1;
    @BindView(R.id.main_root)
    LinearLayout linearLayout;
    @BindView(R.id.main_view_pager)
    ViewPager viewPager;
    @BindView(R.id.main_tabLayout)
    TabLayout tabLayout;

    FusedLocationProviderClient fusedLocationProviderClient;
    LocationCallback locationCallback;
    LocationRequest locationRequest;
    ViewPagerAdapter viewPagerAdapter;
    CompositeDisposable compositeDisposable;
    AccuWeather accuWeather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        compositeDisposable = new CompositeDisposable();
        Retrofit retrofit = RetrofitClient.getClient();
        accuWeather = retrofit.create(AccuWeather.class);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_ACCESS_LOCATION);
        } else {
            setUpLocationClient();
        }
    }

    @SuppressLint("MissingPermission")
    void setUpLocationClient() {
        buildLocationRequest();
        buildLocationCallBack();

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);
        fusedLocationProviderClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.myLooper()
        );
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, getString(R.string.permissions_not_granted), Snackbar.LENGTH_LONG).show();
                return;
            }
        }
        setUpLocationClient();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    private void buildLocationCallBack() {
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Vars.currentLocation = locationResult.getLastLocation();
                String latLon = Vars.currentLocation.getLatitude() + "," + Vars.currentLocation.getLongitude();
                compositeDisposable.clear();
                compositeDisposable.add(
                        accuWeather.getLocationInfo(Vars.API_KEY, latLon, false)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        locationInfo -> {
                                            Vars.locationInfo = locationInfo;
                                            setUpViewPager();
                                        },
                                        throwable -> {
                                            Toast.makeText(MainActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                )
                );
            }
        };
    }

    @OnClick(R.id.main_refresh)
    void setUpViewPager() {
        if (Vars.currentLocation != null && Vars.locationInfo != null) {
            viewPagerAdapter.removeAll();
            viewPagerAdapter.addItem(new CurrentWeatherFragment(), getString(R.string.current_conditions));
            viewPagerAdapter.addItem(new ForecastWeatherFragment(), getString(R.string.forecast));
            viewPager.setAdapter(viewPagerAdapter);
            tabLayout.setupWithViewPager(viewPager);
        } else {
            Toast.makeText(MainActivity.this, getString(R.string.unknown_location), Toast.LENGTH_SHORT).show();
        }
    }

    private void buildLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setInterval(60000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setSmallestDisplacement(500);
    }
}
