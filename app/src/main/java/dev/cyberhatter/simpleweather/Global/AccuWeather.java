package dev.cyberhatter.simpleweather.Global;

import dev.cyberhatter.simpleweather.Model.FiveDaysForecastInfo;
import dev.cyberhatter.simpleweather.Model.HalfDayForecastInfo;
import dev.cyberhatter.simpleweather.Model.LocationInfo;
import dev.cyberhatter.simpleweather.Model.CurrentWeatherInfo;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AccuWeather {

    @GET("locations/v1/cities/geoposition/search")
    Observable<LocationInfo> getLocationInfo(
            @Query("apikey") String apiKey,
            @Query("q") String latLon,
            @Query("toplevel") Boolean topLevel
    );

    @GET("currentconditions/v1/{locationKey}")
    Observable<CurrentWeatherInfo[]> getWeatherInfo(
            @Path("locationKey") String locationKey,
            @Query("apikey") String apiKey
    );

    @GET("forecasts/v1/hourly/12hour/{locationKey}")
    Observable<HalfDayForecastInfo[]> get12HourForecast(
            @Path("locationKey") String locationKey,
            @Query("apikey") String apiKey,
            @Query("metric") Boolean metric
    );

    @GET("forecasts/v1/daily/5day/{locationKey}")
    Observable<FiveDaysForecastInfo> get5DaysForecast(
            @Path("locationKey") String locationKey,
            @Query("apikey") String apikey,
            @Query("metric") Boolean metric
    );
}
